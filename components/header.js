import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import { Col, Container, Image, Row } from "react-bootstrap";
import MobileMenu from "./mobileMenu";
import Link from "next/link";
import DesktopMenu from "./desktopMenu";
import Auth from "@aws-amplify/auth";
import { Media } from "../helper/Media";

const Header = observer(() => {
  const [loggedIn, setLoggedIn] = useState(false);
  useEffect(() => {
    try {
      Auth.currentAuthenticatedUser();
      setLoggedIn(true);
    } catch (e) {}
  });
  return (
    <Container fluid>
      <Row style={{ maxWidth: "100vw" }}>
        <Media at="xs">
          <Col
            style={{
              backgroundColor: "white",
              position: "fixed",
              top: "0",
              left: "0",
              width: "100%",
              maxWidth: "100vw",
              zIndex: "1",
              height: "150px",
              borderBottom: "0.5px solid lightgray",
            }}
            className="d-flex justify-content-center align-items-center"
          >
            <Link href="/" passHref>
              <a>
                <Image src="/logo_oneline.PNG" style={{ height: "150px" }} />
              </a>
            </Link>
            <MobileMenu loggedIn={loggedIn} />
          </Col>
        </Media>
        <Media greaterThan="xs">
          <Col
            className="d-flex justify-content-center"
            style={{
              height: "70px",
              borderBottom: "0.5px solid lightgray",
            }}
          >
            <div style={{ width: "100%", maxWidth: "1366px" }}>
              <Row>
                <Col style={{ maxWidth: "100px" }}>
                  <Link href="/" passHref>
                    <a>
                      <Image src="/logosmall.PNG" fluid />
                    </a>
                  </Link>
                </Col>
                <Col className="d-flex justify-content-end align-items-center">
                  <DesktopMenu loggedIn={loggedIn} />
                </Col>
              </Row>
            </div>
          </Col>
        </Media>
      </Row>
    </Container>
  );
});
export default Header;
