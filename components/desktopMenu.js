import React from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { Dropdown } from 'react-bootstrap';

const Li = styled.div`
  margin-left: 10px;
  margin-right: 20px;
  position: relative;
  a,
  a:hover,
  a:focus,
  a:active,
  a:visited {
    color: #555;
    text-decoration: none;
  }
  a::after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 0.1em;
    background-color: #555;
    opacity: 0;
    transition: opacity 300ms, transform 300ms;
  }
  a:hover::after,
  a:focus::after {
    opacity: 1;
  }

  cursor: pointer;
  font-size: 15pt;
`;

export default function DesktopMenu({ loggedIn }) {
  return (
    <>
      <Li>
        <Link href="/auctions">
          <a>Auktionen</a>
        </Link>
      </Li>
      <Li>
        <Link href="/">
          <a>Verkaufen lassen</a>
        </Link>
      </Li>
      <Li>
        <Dropdown>
          <Dropdown.Toggle id="menudropdown" size="sm">
            <FontAwesomeIcon icon={faUserAlt} />
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item href="#/action-1">
              Meine Gebote
            </Dropdown.Item>
            <Dropdown.Item href="/my">Meine Remise</Dropdown.Item>
            <Dropdown.Item href="#/action-3">
              Meine Inserate
            </Dropdown.Item>
            <Dropdown.Item href="#/action-2">
              Zahlungsinformationen
            </Dropdown.Item>
            <Dropdown.Item href="#/action-3">
              Mein Account
            </Dropdown.Item>
            <hr />
            <Dropdown.Item href="#/action-2">Logout</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Li>
      {!loggedIn && (
        <Li>
          <Link href="/login">
            <a id="conatact" className="menu-item m-3">
              <FontAwesomeIcon
                style={{ width: '20px' }}
                icon={faUser}
              />{' '}
              Login
            </a>
          </Link>
          <br />
          <br />
        </Li>
      )}
    </>
  );
}
