import {
  faBars,
  faEnvelope,
  faGavel,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import Link from "next/link";
import { slide as Menu } from "react-burger-menu";
import { Button, Image } from "react-bootstrap";

export default function MobileMenu(loggedIn) {
  const [menuOpen, setMenuOpen] = useState(false);
  return (
    <>
      <Menu width={"100%"} isOpen={menuOpen} right>
        <div className="w-100 text-center mb-5">
          <Image src="/logosmall.PNG" />
          <br />
          <Link href="/">
            <a className="w-100 text-center">
              <Button variant="primary" size="lg">
                Jetzt verkaufen
              </Button>
            </a>
          </Link>
        </div>
        <Link href="/auctions">
          <a className="menu-item m-3">
            <FontAwesomeIcon style={{ width: "20px" }} icon={faGavel} />{" "}
            Auktionen
          </a>
        </Link>
        <br />
        <br />
        {!loggedIn && (
          <>
            <Link href="/login">
              <a id="conatact" className="menu-item m-3">
                <FontAwesomeIcon style={{ width: "20px" }} icon={faUser} />{" "}
                Login
              </a>
            </Link>
            <br />
            <br />
          </>
        )}
        <Link href="/contact">
          <a id="contaact" href="/contact" className="menu-item m-3">
            <FontAwesomeIcon style={{ width: "20px" }} icon={faEnvelope} />{" "}
            Kontakt
          </a>
        </Link>
      </Menu>
    </>
  );
}
