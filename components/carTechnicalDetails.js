import { observer } from "mobx-react-lite";
import React from "react";
import { Col, Row } from "react-bootstrap";

const CarTechnicalDetails = observer(({ store }) => {
  return (
    <Row>
      <Col>
        <div
          className="w-100 rounded mt-2 p-2 "
          style={{ backgroundColor: "rgb(245, 245, 245)" }}
        >
          <h5>Fahrzeugdaten</h5>
          <div className="d-flex align-items-center my-2">
            <img
              src="/Kilometer.svg"
              style={{ height: "25px", marginRight: "10px" }}
            />
            {store.auction.km}km
          </div>
          <div className="d-flex align-items-center my-2">
            <img
              src="/Aussenfarbe.svg"
              style={{ height: "25px", marginRight: "10px" }}
            />
            {store.auction.colorOut} (Außenfarbe)
          </div>
          <div className="d-flex align-items-center my-2">
            <img
              src="/Innenfarbe.svg"
              style={{ height: "25px", marginRight: "10px" }}
            />
            {store.auction.colorOut} (Innenfarbe)
          </div>
          <div className="d-flex align-items-center my-2">
            <img
              src="/Motor.svg"
              style={{ height: "25px", marginRight: "10px" }}
            />
            {store.auction.motor}
          </div>
          <div className="d-flex align-items-center my-2">
            <img
              src="/Getriebe.svg"
              style={{ height: "25px", marginRight: "10px" }}
            />
            {store.auction.getriebe}
          </div>
          <div className="d-flex align-items-center my-2">
            <img
              src="/FIN.png"
              style={{ height: "25px", marginRight: "10px" }}
            />
            {store.auction.fin}
          </div>
          <hr />
          <b>Kaufpremium</b>: {store.auction.commission}%
        </div>
      </Col>
    </Row>
  );
});
export default CarTechnicalDetails;
