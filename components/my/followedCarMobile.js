import { observer } from 'mobx-react-lite';
import { Col, Row } from 'react-bootstrap';
import React from 'react';
import Countdown from 'react-countdown';
import Link from 'next/link';

const FollowedCarMobile = observer(({ car }) => {
  return (
    <div className="w-100 p-3">
      <Link href={`/auction/${car.id}`}>
        <a>
          <div className="w-100">
            <Row>
              <img
                src={car.thumbnail}
                style={{ width: '100%' }}
                className="p-0"
              />
            </Row>
            <Row
              style={{
                backgroundColor: 'rgb(61, 93, 84)',
                borderRadius: '5px',
                height: '40px',
                color: 'white',
                fontSize: '20px',
              }}
            >
              <Col className="d-flex justify-content-center align-items-center">
                <Countdown date={car.end} daysInHours />
              </Col>
              <Col className="d-flex justify-content-center align-items-center">
                {car.current_bid.toFixed(2).replace('.', ',')}€
              </Col>
            </Row>
            <Row
              style={{ backgroundColor: 'rgba(189, 187, 185, 0.15)' }}
            >
              <Col
                className="text-center pt-2"
                style={{ fontSize: '20px' }}
              >
                <b>
                  {car.brand} {car.model}
                  <br />({car.year})
                </b>
                <br />
                <span style={{ fontSize: '16px' }}>
                  {car.description}
                </span>
              </Col>
            </Row>
          </div>
        </a>
      </Link>
    </div>
  );
});
export default FollowedCarMobile;
