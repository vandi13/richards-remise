import { observer } from "mobx-react-lite";
import React, { useEffect } from "react";
import { Col, Row, Spinner } from "react-bootstrap";
import FollowedAuctionsStore from "../../store/FollowedAuctionsStore";
import FollowedCarMobile from "./followedCarMobile";

const followedAuctionsStore = new FollowedAuctionsStore();

const MeineRemiseMobile = observer(({ userStore }) => {
  useEffect(() => {
    followedAuctionsStore.init(userStore.follows);
  }, []);
  return (
    <>
      <Row>
        <Col className="text-center pt-3">
          <h3>Meine Auktionen</h3>
          <p className="text-start">
            Hier sehen Sie alle gespeicherten Fahrzeuge aus der Remise, sowie
            Auktionen mit Ihren Geboten:
          </p>
        </Col>
      </Row>
      {!followedAuctionsStore.ready && (
        <Row>
          <Col className="d-flex justify-content-center">
            {/* <Spinner animation="border" /> */}
          </Col>
        </Row>
      )}
      {followedAuctionsStore.ready && (
        <Row>
          <Col>
            {followedAuctionsStore.followedAuctions.map((auction) => {
              return (
                <div key={`fa${auction.id}`} className="w-100">
                  <FollowedCarMobile car={auction} />
                </div>
              );
            })}
          </Col>
        </Row>
      )}
    </>
  );
});
export default MeineRemiseMobile;
