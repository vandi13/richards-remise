import { observer } from 'mobx-react-lite';
import React from 'react';
import { Media } from '../../helper/Media';
import MeineRemiseMobile from './meineRemiseMobile';

const ProfileView = observer(({ userStore }) => {
  return (
    <>
      <div
        style={{ height: '100px', backgroundColor: '#5F5E5D' }}
        className="w-100 mt-2"
      ></div>
      <div
        style={{
          marginTop: '-50px',
          paddingLeft: '20px',
          fontSize: '20px',
        }}
      >
        <div
          style={{
            borderRadius: '50%',
            width: '80px',
            height: '80px',
            backgroundColor: 'lightgray',
            fontSize: '30px',
            color: 'gray',
          }}
          className="d-flex justify-content-center align-items-center
          "
        >
          {userStore.user.username.slice(0, 1).toUpperCase()}
        </div>
        <p style={{ position: 'relative', left: '5px' }}>
          {userStore.user.username}
        </p>
      </div>
      <Media at="xs">
        <MeineRemiseMobile userStore={userStore} />
      </Media>
      <Media greaterThan="xs">
        <p>lol</p>
      </Media>
    </>
  );
});
export default ProfileView;
