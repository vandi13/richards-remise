import { observer } from "mobx-react-lite";
import React from "react";
import { Col, Row } from "react-bootstrap";
import Countdown from "react-countdown";

const BidInfoRow = observer(({ store }) => {
  return (
    <Row className="mt-2" style={{ fontSize: "16px" }}>
      <Col
        xs={4}
        className="d-flex justify-content-center flex-column align-items-center"
      >
        <span style={{ fontSize: "18px" }}>
          <b>{store.auction.current_bid}€</b>
        </span>
        <span style={{ fontSize: "12px" }}>aktuelles Gebot</span>
      </Col>
      <Col
        xs={4}
        className="d-flex justify-content-center flex-column align-items-center"
      >
        <Countdown date={store.auction.end} daysInHours />
        <span style={{ fontSize: "12px" }}>Restzeit</span>
      </Col>
      <Col
        xs={4}
        className="d-flex justify-content-center flex-column align-items-center"
      >
        {store.auction.bids}
        <span style={{ fontSize: "12px" }}>Gebote</span>
      </Col>
    </Row>
  );
});
export default BidInfoRow;
