import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { observer } from "mobx-react-lite";
import React from "react";
import { Col, Form, Row, Button, Spinner } from "react-bootstrap";

const BidInput = observer(({ bidStore, store }) => {
  return (
    <div
      style={{
        position: "fixed",
        zIndex: "5",
        bottom: "0",
        left: "0",
        width: "100vw",
        height: "150px",
        backgroundColor: "white",
      }}
    >
      <Row>
        <Col style={{ color: "gray" }} className="px-4">
          Minimum:{" "}
          {parseFloat(store.auction.current_bid) >= 20000 ? (
            `${parseFloat(store.auction.current_bid) + 500} €`
          ) : (
            <>
              {parseFloat(store.auction.current_bid) >= 5000
                ? `${parseFloat(store.auction.current_bid) + 250} €`
                : `${parseFloat(store.auction.current_bid) + 100} €`}
            </>
          )}
        </Col>
        <Col className="text-end px-4">
          <span onClick={() => bidStore.setInputVisible(false)}>Schließen</span>
        </Col>
      </Row>
      <Row>
        <Col className="px-4 mt-2">
          <Form.Control
            type="number"
            placeholder="Gebot eingeben"
            onChange={(e) => bidStore.setBidAmount(e.target.value)}
          />
        </Col>
      </Row>
      <Row>
        <Col>
          <div className="d-grid gap-2 mt-2 px-2 py-2">
            <Button
              variant="primary"
              size="lg"
              disabled={bidStore.bidSending || bidStore.bidSentSuccessfully}
              onClick={() => bidStore.sendBid(store.auction.id)}
            >
              {bidStore.bidSending ? (
                <>
                  <Spinner animation="border" size="sm" as="span" />
                </>
              ) : (
                <>
                  {bidStore.bidSentSuccessfully ? (
                    <span>
                      <FontAwesomeIcon icon={faCheck} /> Gebot abgegeben
                    </span>
                  ) : (
                    <span style={{ fontSize: "16px" }}>
                      {bidStore.bidMode === "live" ? "Live" : "Maximal"}gebot
                      abgeben
                    </span>
                  )}
                </>
              )}
            </Button>
          </div>
        </Col>
      </Row>
    </div>
  );
});
export default BidInput;
