import React from "react";
import Link from "next/link";

export default function MobileFooter() {
  return (
    <>
      <div className="w-100 text-center">
        <h3>Über uns</h3>
      </div>
      <div className="px-3 mt-2">
        <div>
          <Link href="/faq">
            <a>FAQ</a>
          </Link>
        </div>
        <div>
          <Link href="/faq">
            <a>Impressum</a>
          </Link>
        </div>
        <div>
          <Link href="/faq">
            <a>Datenschutz</a>
          </Link>
        </div>
        <div>
          <Link href="/faq">
            <a>AGB</a>
          </Link>
        </div>
      </div>
    </>
  );
}
