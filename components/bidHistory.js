import { observer } from "mobx-react-lite";
import React from "react";
import { Col, Row, Spinner } from "react-bootstrap";
import Moment from "react-moment";

const BidHistory = observer(({ store }) => {
  if (!store.bidHistoryReady) {
    return (
      <>
        <Spinner animation="border" />
      </>
    );
  }

  return (
    <div
      className="my-3"
      style={{
        overflowX: "hidden",
        overflowY: "scroll",
        maxHeight: "calc( 100vh - 300px )",
      }}
    >
      <h5 className="mb-3">Gebotshistorie:</h5>
      {store.bidHistory.map((bid, i) => {
        return (
          <Row className="my-2" key={`bdhi${i}`}>
            <Col className="px-5" style={{ fontSize: "14px" }}>
              <span style={{ fontSize: "18px" }}>
                <b>{bid.bid_amount}€</b>
              </span>
              <br />
              <span>Gebot von {bid.username}</span>
              <br />
              <span>
                <Moment fromNow locale="de">
                  {bid.timestamp}
                </Moment>
              </span>
              {i === 0 && <hr color="lightgray" />}
            </Col>
          </Row>
        );
      })}
    </div>
  );
});
export default BidHistory;
