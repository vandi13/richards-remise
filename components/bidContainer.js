import {
  faTimes,
  faWindowClose,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { observer } from 'mobx-react-lite';
import React, { useState } from 'react';
import { Media } from '../helper/Media';
import BidStore from '../store/BidStore';
import MobileBidContainer from './MobileBidContainer';

const bidStore = new BidStore();

const BidContainer = observer(({ store, setShowBidContainer }) => {
  return (
    <>
      <Media at="xs">
        <MobileBidContainer
          store={store}
          setShowBidContainer={setShowBidContainer}
          bidStore={bidStore}
        />
      </Media>
    </>
  );
});
export default BidContainer;
