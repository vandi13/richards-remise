import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { observer } from "mobx-react-lite";
import React from "react";
import { Col, Container, Image, Row, Button } from "react-bootstrap";
import styled from "styled-components";
import BidHistory from "./bidHistory";
import BidInfoRow from "./bidInfoRow";
import BidInput from "./bidInput";
import FollowBox from "./followBox";

const MobileBidContainer = observer(
  ({ store, setShowBidContainer, bidStore }) => {
    return (
      <>
        <div
          style={{
            position: "fixed",
            top: "0",
            left: "0",
            width: "100vw",
            height: "100vh",
            backgroundColor: "white",
            zIndex: "2",
          }}
        >
          <div
            style={{
              height: "calc( 100% - 100px )",
              overflowY: "hidden",
              overflowX: "hidden",
            }}
          >
            <FontAwesomeIcon
              icon={faTimes}
              size="lg"
              style={{ position: "fixed", top: "5px", right: "10px" }}
              onClick={() => setShowBidContainer(false)}
            />
            <BidInfoRow store={store} />
            <hr style={{ color: "lightgray" }} />
            <Container fluid>
              <Row>
                <Col>
                  <Image src={store.thumbnail} className="w-100" />
                </Col>
                <Col>
                  <Row>
                    <Col>
                      <b>
                        {store.auction.year} {store.auction.brand}{" "}
                        {store.auction.model}
                      </b>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <FollowBox
                        style={{ justifyContent: "start" }}
                        store={store}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row>
                <Col>
                  <BidHistory store={store} />
                </Col>
              </Row>
            </Container>
          </div>
          <div style={{ height: "100px" }}>
            <Row className="h-100">
              <Col>
                <div className="d-grid gap-2 mt-2 px-2 py-2">
                  <Button
                    variant="primary"
                    size="lg"
                    onClick={() => {
                      bidStore.setBidMode("live");
                      bidStore.setInputVisible(true);
                    }}
                  >
                    <span style={{ fontSize: "14px" }}>Live-Gebot abgeben</span>
                  </Button>
                </div>
              </Col>
              <Col>
                <div className="d-grid gap-2 mt-2 px-2 py-2">
                  <Button
                    variant="primary"
                    size="lg"
                    onClick={() => {
                      bidStore.setBidMode("max");
                      bidStore.setInputVisible(true);
                    }}
                  >
                    <span style={{ fontSize: "14px" }}>
                      Maximalgebot abgeben
                    </span>
                  </Button>
                </div>
              </Col>
            </Row>
          </div>
        </div>
        {bidStore.inputVisible && (
          <BidInput bidStore={bidStore} store={store} />
        )}
      </>
    );
  }
);
export default MobileBidContainer;
