import React from "react";
import { observer } from "mobx-react-lite";
import { Col, Row } from "react-bootstrap";
import Countdown from "react-countdown";
import { useMediaQuery } from "react-responsive";

const AuctionListCountdownMobile = observer(({ auction }) => {
  const isMobile = useMediaQuery({ maxWidth: 767 });

  return (
    <div
      style={{
        width: "142px",
        height: "28px",
        backgroundColor: "white",
        borderBottomLeftRadius: "8px",
        borderTopLeftRadius: "8px",
        fontSize: "13px",
      }}
      className="shadow-sm"
    >
      <Row className="h-100">
        <Col className="d-flex align-items-center justify-content-end">
          <Countdown date={auction.auction.end} daysInHours />
        </Col>
        <Col
          style={{ backgroundColor: "#3d5d54", color: "white" }}
          className="d-flex align-items-center justify-content-end"
        >
          <b>{auction.auction.current_bid}€</b>
        </Col>
      </Row>
    </div>
  );
});
export default AuctionListCountdownMobile;
