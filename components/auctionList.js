import React, { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import ApiHelper from "../helper/api";
import { Col, Container, Row, Spinner } from "react-bootstrap";
import AuctionListStore from "../store/AuctionListStore";
import AuctionListItem from "./auctionListItem";
import Link from "next/link";
import AuctionStore from "../store/AuctionStore";

const apiHelper = new ApiHelper();
const auctionListStore = new AuctionListStore();

const AuctionList = observer(({ title }) => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    auctionListStore.clearAuctions();
    apiHelper.getAllAuctions().then((response) => {
      response.body.Items.map((item) => {
        let auction = new AuctionStore(item);
        auctionListStore.addAuction(auction);
      });
      setLoading(false);
    });
  }, [auctionListStore]);

  if (loading) {
    return (
      <Container
        fluid
        className="d-flex justify-content-center align-items-center"
        style={{ height: "calc( 100% - 70px )" }}
      >
        <Spinner animation="border" />
      </Container>
    );
  }

  return (
    <Container size="xxl">
      {title && (
        <Row>
          <Col className="text-center m-3">
            <h2>Unsere Kollektion</h2>
          </Col>
        </Row>
      )}
      <Row>
        <Col className="d-flex justify-content-around flex-wrap">
          {auctionListStore.auctions.map((auction, i) => {
            return (
              <Link href={`/auction/${auction.auction.id}`} key={`asli${i}`}>
                <a>
                  <AuctionListItem auction={auction} />
                </a>
              </Link>
            );
          })}
        </Col>
      </Row>
    </Container>
  );
});
export default AuctionList;
