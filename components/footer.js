import Link from 'next/link';
import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import styled from 'styled-components';
import { Media } from '../helper/Media';
import DesktopFooter from './desktopFooter';
import MobileFooter from './mobileFooter';

const ResponsiveRow = styled(Row)`
  height: 200px;
  backgroundcolor: 'lightgray';
  @media (maxwidth: '767px') {
    height: 250px;
  }
`;

export default function Footer() {
  return (
    <Container fluid>
      <ResponsiveRow>
        <Col className="p-3">
          <Media at="xs">
            <MobileFooter />
          </Media>
          <Media greaterThan="xs">
            <DesktopFooter />
          </Media>
        </Col>
      </ResponsiveRow>
    </Container>
  );
}
