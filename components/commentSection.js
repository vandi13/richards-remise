import { faComment } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { observer } from "mobx-react-lite";
import React, { useState } from "react";
import { Col, Form, Row, Button, Spinner } from "react-bootstrap";
import Moment from "react-moment";
import "moment/locale/de";

const CommentSection = observer(({ store, userStore }) => {
  const [comment, setComment] = useState("");

  const sendComment = () => {
    store.sendComment(comment);
  };

  return (
    <Row>
      <Col>
        <h5>Kommentar abgeben</h5>
        <Form.Control
          as="textarea"
          rows={3}
          onChange={(e) => setComment(e.target.value)}
        />
        <div className="d-grid gap-2 mt-2 mb-3">
          <Button
            variant="secondary"
            disabled={!userStore.loggedIn || userStore.commentSending}
            onClick={sendComment}
          >
            {!store.commentSending ? (
              <>Absenden</>
            ) : (
              <Spinner animation="border" as="span" size="sm" />
            )}
          </Button>
        </div>
        <h5>Kommentare</h5>
        {store.auction.comments
          .slice()
          .reverse()
          .map((comment, c) => {
            return (
              <Row key={`cmnt${c}`} className="my-2">
                <Col xs={1} className="d-flex align-items-center">
                  <FontAwesomeIcon icon={faComment} />
                </Col>
                <Col
                  className="rounded d-flex justify-content-center flex-column"
                  style={{ backgroundColor: "#f6f6f6" }}
                >
                  {comment.Comment}
                  <span style={{ fontSize: "10px" }}>
                    {comment.Username},{" "}
                    <Moment fromNow locale="de">
                      {comment.time}
                    </Moment>
                  </span>
                </Col>
              </Row>
            );
          })}
      </Col>
    </Row>
  );
});
export default CommentSection;
