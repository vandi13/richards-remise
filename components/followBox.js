import { faBookmark, faEye } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { observer } from "mobx-react-lite";
import React from "react";

const FollowBox = observer(({ store, style }) => {
  return (
    <div className="w-100 h-100 d-flex" style={style}>
      <div
        style={{ backgroundColor: "rgb(245, 245, 245)" }}
        className="rounded p-2 mx-2"
      >
        <FontAwesomeIcon icon={faBookmark} /> {store.auction.follows}
      </div>
      <div
        style={{ backgroundColor: "rgb(245, 245, 245)" }}
        className="rounded p-2"
      >
        <FontAwesomeIcon icon={faEye} /> {store.auction.views}
      </div>
    </div>
  );
});
export default FollowBox;
