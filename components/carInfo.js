import { observer } from "mobx-react-lite";
import React from "react";
import { Col, Row } from "react-bootstrap";
import CarDescription from "./carDescription";
import CarTechnicalDetails from "./carTechnicalDetails";
import FollowBox from "./followBox";

const CarInfo = observer(({ store, userStore }) => {
  return (
    <>
      <Row>
        <Col>
          <h2>
            {store.auction.year} {store.auction.brand} {store.auction.model}
          </h2>
        </Col>
      </Row>
      <Row>
        <Col>
          <FollowBox store={store} style={{ justifyContent: "end" }} />
        </Col>
      </Row>
      <CarTechnicalDetails store={store} />
      <CarDescription store={store} />
    </>
  );
});
export default CarInfo;
