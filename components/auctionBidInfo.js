import { faBookmark } from "@fortawesome/free-regular-svg-icons";
import { faCheck, faShareAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import { Col, Row, Button, Spinner } from "react-bootstrap";
import Countdown from "react-countdown";
import { useRouter } from "next/router";
import BidInfoRow from "./bidInfoRow";

const AuctionBidInfo = observer(({ store, userStore, setShowBidContainer }) => {
  const [following, setFollowing] = useState(false);
  const router = useRouter();

  useEffect(() => {
    if (store.auction.id === undefined) return;
    if (
      userStore.follows.filter((follow) => follow.id === store.auction.id)
        .length > 0
    ) {
      setFollowing(true);
    }
  }, [store.auction.id]);

  const doFollow = () => {
    if (!userStore.loggedIn) return;
    userStore.followAuction(store.auction.id);
    setFollowing(true);
  };

  const doUnfollow = () => {
    if (!userStore.loggedIn) return;
    userStore.unfollowAuction(store.auction.id);
    setFollowing(false);
  };

  return (
    <>
      <BidInfoRow store={store} />
      <div className="d-grid gap-2 mt-2">
        {!userStore.loggedIn ? (
          <Button
            variant="primary"
            size="lg"
            onClick={() => router.push("/login")}
          >
            Zum Bieten anmelden
          </Button>
        ) : (
          <Button
            variant="primary"
            size="lg"
            onClick={() => setShowBidContainer(true)}
          >
            Gebot abgeben
          </Button>
        )}
      </div>
      <Row className="mt-2">
        <Col>
          <div
            className="w-100 rounded d-flex justify-content-center align-items-center"
            style={{
              backgroundColor: "#f5f5f5",
              border: "1px solid #dedede ",
              height: "35px",
            }}
            onClick={!userStore.loggedIn || !following ? doFollow : doUnfollow}
          >
            {!userStore.followLoading ? (
              <>
                {!userStore.loggedIn || !following ? (
                  <FontAwesomeIcon
                    icon={faBookmark}
                    size="lg"
                    className="mx-2"
                  />
                ) : (
                  <>
                    <FontAwesomeIcon
                      icon={faCheck}
                      size="lg"
                      className="mx-2"
                    />{" "}
                    folge
                  </>
                )}
              </>
            ) : (
              <>
                <Spinner animation="border" as="span" size="sm" />
              </>
            )}
          </div>
        </Col>
        <Col>
          <div
            className="w-100 rounded d-flex justify-content-center align-items-center"
            style={{
              backgroundColor: "#f5f5f5",
              border: "1px solid #dedede ",
              height: "35px",
            }}
          >
            <FontAwesomeIcon icon={faShareAlt} size="lg" />
          </div>
        </Col>
      </Row>
    </>
  );
});

export default AuctionBidInfo;
