import {
  faBookmark,
  faDoorClosed,
  faDoorOpen,
  faHome,
  faHouse,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { observer } from "mobx-react-lite";
import React from "react";
import Link from "next/link";
import { Col, Container, Row } from "react-bootstrap";
import Auth from "@aws-amplify/auth";

const MobileBottomMenu = observer(({}) => {
  return (
    <div
      style={{
        position: "fixed",
        bottom: "0px",
        left: "0px",
        height: "40px",
        width: "100vw",
        borderTop: "1px solid lightgray",
        backgroundColor: "white",
      }}
    >
      <Container fluid className="h-100">
        <Row className="h-100">
          <Col className="h-100 d-flex justify-content-center align-items-center">
            <Link href="/">
              <a>
                <FontAwesomeIcon icon={faHome} size="lg" />
              </a>
            </Link>
          </Col>
          <Col className="h-100 d-flex justify-content-center align-items-center">
            <Link href="/my">
              <a>
                <FontAwesomeIcon icon={faBookmark} size="lg" />
              </a>
            </Link>
          </Col>
          <Col className="h-100 d-flex justify-content-center align-items-center">
            <FontAwesomeIcon icon={faUser} size="lg" />
          </Col>
          <Col className="h-100 d-flex justify-content-center align-items-center">
            <FontAwesomeIcon
              icon={faDoorOpen}
              size="lg"
              onClick={() => {
                Auth.signOut();
              }}
            />
          </Col>
        </Row>
      </Container>
    </div>
  );
});
export default MobileBottomMenu;
