import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { Media } from "../helper/Media";
import Footer from "./footer";
import Header from "./header";
import MobileBottomMenu from "./mobileBottomMenu";

export default function SiteWrapper({ children }) {
  return (
    <>
      <Header />
      <div
        style={{
          paddingTop: "150px",
          paddingBottom: "40px",
          height: "auto",
          minHeight: "calc( 100% - 40px )",
        }}
      >
        {children}
      </div>
      <Media at="xs">
        <MobileBottomMenu />
      </Media>
      <Media greaterThan="xs">
        <Footer />
      </Media>
    </>
  );
}
