import React from "react";
import Link from "next/link";
import styled from "styled-components";
import { Card } from "react-bootstrap";

const FooterItem = styled(Card)`
  border: 0px;
  h1 {
    font-size: 22pt;
  }
  text-align: center;
  a,
  a:hover {
    color: black;
    text-decoration: none;
  }
  background-color: lightgray;
`;
const FooterWrapper = styled.div`
  width: 100%;
  max-width: 1366px;
  padding-top: 50px;
  padding-bottom: 60px;
  height: 300px;
`;

export default function DesktopFooter() {
  return (
    <div className="d-flex justify-content-around">
      <FooterItem>
        <h1>Links</h1>
        <br />
        <Link href="/">
          <a>AGB</a>
        </Link>
        <Link href="/">
          <a>Widerrufsbelehrung</a>
        </Link>
        <Link href="/">
          <a>Impressum</a>
        </Link>
        <Link href="/">
          <a>Datenschutz</a>
        </Link>
      </FooterItem>
      <FooterItem>
        <h1>Remise</h1>
        <br />
        <Link href="/">
          <a>Auktionen</a>
        </Link>
        <Link href="/">
          <a>Wie verkaufe ich?</a>
        </Link>
        <Link href="/">
          <a>Wie kaufe ich?</a>
        </Link>
        <Link href="/">
          <a>Gebühren</a>
        </Link>
        <Link href="/">
          <a>FAQ</a>
        </Link>
      </FooterItem>
      <FooterItem>
        <h1>Follow Us</h1>
      </FooterItem>
    </div>
  );
}
