import { observer } from "mobx-react-lite";
import React from "react";
import { Col, Row } from "react-bootstrap";

const CarDescription = observer(({ store }) => {
  return (
    <Row>
      <Col>
        <div
          className="w-100 mt-3"
          dangerouslySetInnerHTML={{ __html: store.auction.page_html }}
        />
      </Col>
    </Row>
  );
});
export default CarDescription;
