import { observer } from "mobx-react-lite";
import React from "react";
import { Image } from "react-bootstrap";
import AuctionList from "../auctionList";

const HomeMobile = observer(({}) => {
  return (
    <>
      <div
        className="w-100 d-flex justify-content-center align-items-center flex-column"
        style={{
          height: "565px",
          backgroundImage: "url('/bigbg.webp')",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
        }}
      >
        <Image
          src="/logobig.webp"
          style={{ width: "280px", height: "198px" }}
        />
        <span
          style={{ fontSize: "24px", color: "#3D5D54" }}
          className="text-center my-4"
        >
          Einfach Ihren Klassiker verkaufen lassen
        </span>
        <Image src="/partner1.webp" className="my-4" />
        <Image src="/partner2.webp" className="my-4" />
      </div>
      <div className="w-100 text-center py-5 px-3">
        <h1>UNSERE KOLLEKTION</h1>
        <p style={{ fontSize: "20px" }} className="mt-4">
          Schauen Sie sich gerne um, inspizieren Sie unsere Exponate. In
          Richards Remise bieten wir Ihnen einen Feinkostladen für
          Automobilisten, eine Boutique für jeden Benzin-Liebhaber. Von Fotos,
          über Videos bis hin zu 3D Ausstellung des Innenraums ist alles dabei.
        </p>
      </div>
      <AuctionList title={false} />
    </>
  );
});
export default HomeMobile;
