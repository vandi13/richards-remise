import { observer } from "mobx-react-lite";
import React, { useState } from "react";
import { Card, Spinner } from "react-bootstrap";
import { useMediaQuery } from "react-responsive";
import AuctionListCountdownMobile from "./auctionListCountdownMobile";

const AuctionListItem = observer(({ auction }) => {
  const [loading, setLoading] = useState(true);
  const isMobile = useMediaQuery({ maxWidth: 767 });

  if (!auction.ready) {
    return (
      <Card
        style={{ width: "180px", height: "380px" }}
        className="d-flex justify-content-center align-items-center "
      >
        <Spinner animation="border" />{" "}
      </Card>
    );
  }

  return (
    <Card
      style={{
        width: isMobile ? "100%" : "510px",
        minWidth: isMobile ? "100%" : "510px",
        height: isMobile ? "300px" : "600px",
        border: "none",
        overflow: "hidden",
      }}
      className="p-2 text-center"
    >
      <Card.Img
        variant="top"
        src={auction.thumbnail}
        style={{
          width: isMobile ? "100%" : "510px",
          height: isMobile ? "auto" : "400px",
        }}
        className="rounded "
      />
      <AuctionListCountdownMobile auction={auction} />
      <Card.Body className="p-0">
        <div
          className="text-start mt-4"
          style={{
            backgroundColor: "rgb(245, 245, 245)",
            fontSize: "12px",
            paddingLeft: "5px",
            width: "80px",
          }}
        >
          {auction.auction.bids} Gebote
        </div>
        <div className="text-start">
          <b>
            {auction.auction.year} {auction.auction.brand}{" "}
            {auction.auction.model}
          </b>
        </div>
        <div className="text-start" style={{ color: "#666", fontSize: "14px" }}>
          {auction.auction.description}
        </div>
      </Card.Body>
    </Card>
  );
});
export default AuctionListItem;
