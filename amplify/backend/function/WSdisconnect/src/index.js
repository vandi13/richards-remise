/* Amplify Params - DO NOT EDIT
	ENV
	REGION
	STORAGE_SERVERCONNECTIONS_ARN
	STORAGE_SERVERCONNECTIONS_NAME
Amplify Params - DO NOT EDIT */
const AWS = require("aws-sdk");
const ddb = new AWS.DynamoDB.DocumentClient({
  region: "eu-central-1",
});
const CON_TABLE = "serverconnections-staging";

exports.handler = async (event) => {
  return new Promise(async (resolve, reject) => {
    //get auction id
    let connectionId = event.requestContext.connectionId;
    let index_request = await ddb
      .query({
        TableName: CON_TABLE,
        IndexName: "clientconnections",
        KeyConditionExpression: "connectionId = :conId",
        ExpressionAttributeValues: {
          ":conId": connectionId,
        },
      })
      .promise()
      .catch((err) => {
        console.log(err);
        reject({
          statusCode: 500,
          body: err,
        });
      });
    let auction_id = index_request.Items[0].auction_id;
    await ddb
      .delete({
        TableName: CON_TABLE,
        Key: {
          auction_id: auction_id,
          connectionId: connectionId,
        },
      })
      .promise()
      .catch((err) => {
        console.log(err);
        reject({
          statusCode: 500,
          body: err,
        });
      });
    resolve({
      statusCode: 200,
      body: "success",
    });
  });
};
