/* Amplify Params - DO NOT EDIT
	ENV
	REGION
	STORAGE_DYNAMO0110890FSTAGING_ARN
	STORAGE_DYNAMO0110890FSTAGING_NAME
	STORAGE_SERVERCONNECTIONS_ARN
	STORAGE_SERVERCONNECTIONS_NAME
Amplify Params - DO NOT EDIT */
var AWS = require("aws-sdk");
AWS.config.update({ region: "eu-central-1" });
var documentClient = new AWS.DynamoDB.DocumentClient();
var moment = require("moment-timezone");
var cwe = new AWS.CloudWatchEvents();
moment().tz("Europe/Berlin").format();
const apiGw = new AWS.ApiGatewayManagementApi({
  endpoint: "i3xsiueai3.execute-api.eu-central-1.amazonaws.com/production",
});
const CONS_TABLE = "serverconnections-staging";

const AUCTIONS_TABLE = "dynamo0110890f-staging";

exports.handler = async (event) => {
  let auction_id = event.auction_id;
  let auction = await documentClient
    .get({
      TableName: AUCTIONS_TABLE,
      Key: { id: auction_id },
    })
    .promise();

  if (auction.Item === null || auction.Item === undefined) return;

  let time = moment.tz(auction.Item.end, "Europe/Berlin");
  console.log("time : " + time.format());

  let current_time = moment().tz("Europe/Berlin");
  console.log("current time: " + current_time.format());

  let diff = time.diff(current_time, "seconds");
  console.log("diff: " + diff);

  if (diff >= 120) {
    console.log("no reset needed");
    return;
  }

  console.log("reset needed");

  current_time = current_time.add("122", "seconds");
  console.log(current_time.format());

  await documentClient
    .update({
      TableName: AUCTIONS_TABLE,
      Key: { id: auction_id },
      UpdateExpression: "SET #end = :nend",
      ExpressionAttributeValues: {
        ":nend": current_time.format("YYYY-MM-DDTHH:mm:ss"),
      },
      ExpressionAttributeNames: {
        "#end": "end",
      },
    })
    .promise()
    .catch((err) => {
      console.log(err);
    });

  let data = await documentClient
    .query({
      TableName: CONS_TABLE,
      KeyConditionExpression: "auction_id = :aid",
      ExpressionAttributeValues: {
        ":aid": auction_id,
      },
    })
    .promise();

  let promises = [];
  data.Items.map((con) => {
    promises.push(
      new Promise(async (presolve, preject) => {
        await apiGw
          .postToConnection({
            ConnectionId: con.connectionId,
            Data: JSON.stringify({
              action: "time_update",
              time: current_time.format("YYYY-MM-DDTHH:mm:ss"),
            }),
          })
          .promise();
        presolve();
      })
    );
  });
  await Promise.all(promises);

  current_time = current_time.add(1, "minute");
  let cron = "cron(" + current_time.utc().format("m H D M ? YYYY") + ")";
  var cweparams = {
    Name: auction_id + "-end",
    RoleArn: "arn:aws:iam::715100531220:role/cwevents",
    ScheduleExpression: cron,
    State: "ENABLED",
  };

  console.log(cweparams);
  await cwe
    .putRule(cweparams)
    .promise()
    .catch((err) => {
      console.log(err);
    });

  return {
    statusCode: 200,
    body: JSON.stringify("Hello from Lambda!"),
  };
};
