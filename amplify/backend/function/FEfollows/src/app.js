/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

var express = require("express");
var bodyParser = require("body-parser");
var awsServerlessExpressMiddleware = require("aws-serverless-express/middleware");
var AWS = require("aws-sdk");
AWS.config.update({ region: "eu-central-1" });
var documentClient = new AWS.DynamoDB.DocumentClient();

var app = express();
app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());

// Enable CORS for all methods
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

/**********************
 * Example get method *
 **********************/

app.get("/follows", function (req, res) {
  // Add your code here
  res.json({ success: "get call succeed!", url: req.url });
});

app.get("/follows/:username", function (req, res) {
  // Add your code here
  return new Promise((resolve, reject) => {
    let username = req.params.username;
    let tableName = "dynamo29f3aeb7-staging";
    documentClient
      .get({
        TableName: tableName,
        Key: { username: username },
      })
      .promise()
      .then((gres) => {
        res.json({ success: "true", url: req.url, body: gres });
        resolve(true);
      })
      .catch((err) => {
        res.json({ success: "false", url: req.url, body: err });
        resolve(true);
      });
  });
});

/****************************
 * Example post method *
 ****************************/

function raiseCount(pId) {
  var tableName = "dynamo0110890f-staging";
  return documentClient
    .update({
      TableName: tableName,
      Key: { id: pId },
      UpdateExpression: "set follows = follows + :fo",
      ExpressionAttributeValues: {
        ":fo": 1,
      },
    })
    .promise();
}

async function lowerCount(pId) {
  var tableName = "dynamo0110890f-staging";
  return documentClient
    .update({
      TableName: tableName,
      Key: { id: pId },
      UpdateExpression: "set follows = follows + :fo",
      ExpressionAttributeValues: {
        ":fo": -1,
      },
    })
    .promise();
}

app.post("/follows", async function (req, res) {
  return new Promise(async (resolve, reject) => {
    // Add your code here
    let action = req.body.action;
    var tableName = "dynamo29f3aeb7-staging";
    if (action === "unfollow") {
      let query_result = await documentClient
        .get({
          TableName: tableName,
          Key: { username: req.body.username },
        })
        .promise()
        .catch((err) => {
          res.json({ success: "false", url: req.url, body: err });
          return;
        });
      if (query_result.Item === undefined || query_result.Item === null) {
        res.json({ success: "false", url: req.url, body: "not_following" });
        return;
      }
      let pFollows = query_result.Item.followed_auctions;
      pFollows = pFollows.filter((f) => f.id !== req.body.auction_id);
      await documentClient
        .update({
          TableName: tableName,
          Key: { username: req.body.username },
          UpdateExpression: "set followed_auctions = :fa",
          ExpressionAttributeValues: {
            ":fa": pFollows,
          },
        })
        .promise()
        .catch((err) => {
          res.json({ success: "false", url: req.url, body: err });
          return;
        });
      await lowerCount(req.body.auction_id).catch((err) => {
        console.log(err);
      });
      res.json({ success: "true", url: req.url, body: "unfollowed" });
      resolve(true);
    }
    if (action === "follow") {
      var params = {
        TableName: tableName,
        Key: {
          username: req.body.username,
        },
      };
      documentClient
        .get(params)
        .promise()
        .then((response) => {
          if (response.Item === undefined) {
            documentClient
              .put({
                TableName: tableName,
                Item: {
                  username: req.body.username,
                  followed_auctions: [{ id: req.body.auction_id }],
                },
              })
              .promise()
              .then((sec_res) => {
                res.json({ success: "true", url: req.url, body: "added" });
                raiseCount(req.body.auction_id)
                  .then((rc) => {
                    resolve(true);
                  })
                  .catch((e) => {
                    console.log(e);
                    resolve(true);
                  });
              })
              .catch((err) => {
                res.json({ success: "false", url: req.url, body: err });
                resolve(true);
              });
          } else {
            let item = response.Item;
            if (
              item.followed_auctions.filter(
                (auc) => auc.id === req.body.auction_id
              ).length > 0
            ) {
              res.json({
                success: "false",
                url: req.url,
                body: "already-following",
              });
              resolve(true);
            } else {
              documentClient
                .update({
                  TableName: tableName,
                  Key: { username: req.body.username },
                  UpdateExpression: "set followed_auctions = :fa",
                  ExpressionAttributeValues: {
                    ":fa": [
                      ...item.followed_auctions,
                      { id: req.body.auction_id },
                    ],
                  },
                })
                .promise()
                .then((thr_res) => {
                  res.json({ success: "true", url: req.url, body: thr_res });
                  raiseCount(req.body.auction_id)
                    .then((rc) => {
                      resolve(true);
                    })
                    .catch((e) => {
                      console.log(e);
                      resolve(true);
                    });
                })
                .catch((err) => {
                  res.json({ success: "false", url: req.url, body: err });
                  resolve(true);
                });
            }
          }
        })
        .catch((err) => {
          res.json({ success: "false", url: req.url, body: err });
          resolve(true);
        });
    }
  });
});

app.post("/follows/*", function (req, res) {
  // Add your code here
  res.json({ success: "post call succeed!", url: req.url, body: req.body });
});

/****************************
 * Example put method *
 ****************************/

app.put("/follows", function (req, res) {
  // Add your code here
  res.json({ success: "put call succeed!", url: req.url, body: req.body });
});

app.put("/follows/*", function (req, res) {
  // Add your code here
  res.json({ success: "put call succeed!", url: req.url, body: req.body });
});

/****************************
 * Example delete method *
 ****************************/

app.delete("/follows", function (req, res) {
  // Add your code here
  res.json({ success: "delete call succeed!", url: req.url });
});

app.delete("/follows/*", function (req, res) {
  // Add your code here
  res.json({ success: "delete call succeed!", url: req.url });
});

app.listen(3000, function () {
  console.log("App started");
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app;
