/* Amplify Params - DO NOT EDIT
	ENV
	REGION
	STORAGE_BIDSHISTORY_ARN
	STORAGE_BIDSHISTORY_NAME
	STORAGE_DYNAMO0110890FSTAGING_ARN
	STORAGE_DYNAMO0110890FSTAGING_NAME
Amplify Params - DO NOT EDIT */
var AWS = require("aws-sdk");
AWS.config.update({ region: "eu-central-1" });
var documentClient = new AWS.DynamoDB.DocumentClient();

const AUCTIONS_TABLE = "dynamo0110890f-staging";

exports.handler = async (event) => {
  // TODO implement
  let auction_id = event.auction_id;
  let total_bid_amounts = event.total_bid_amounts;
  let maxBid = event.maxBid;
  let username = event.username;

  console.log(auction_id);
  console.log(total_bid_amounts);
  console.log(maxBid);
  console.log(username);

  await documentClient
    .update({
      TableName: AUCTIONS_TABLE,
      Key: {
        id: auction_id,
      },
      UpdateExpression:
        "set bids = :bids, current_bid = :cbid, highest_bidder = :hbidder",
      ExpressionAttributeValues: {
        ":bids": total_bid_amounts,
        ":cbid": maxBid,
        ":hbidder": username,
      },
    })
    .promise()
    .catch((err) => {
      console.log(err);
    });

  return;
};
