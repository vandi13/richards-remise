/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

/* Amplify Params - DO NOT EDIT
	ENV
	FUNCTION_LABIDUPDATE_NAME
	FUNCTION_LAMAXBIDUPDATE_NAME
	FUNCTION_LANEWBID_NAME
	FUNCTION_LAPRICEUPDATE_NAME
	FUNCTION_LATIMERRESET_NAME
	REGION
	STORAGE_BIDSHISTORY_ARN
	STORAGE_BIDSHISTORY_NAME
	STORAGE_BIDSHISTORY_STREAMARN
	STORAGE_BIDS_ARN
	STORAGE_BIDS_NAME
	STORAGE_BIDS_STREAMARN
	STORAGE_DYNAMO0110890FSTAGING_ARN
	STORAGE_DYNAMO0110890FSTAGING_NAME
	STORAGE_DYNAMO0110890FSTAGING_STREAMARN
	STORAGE_MAXBIDS_ARN
	STORAGE_MAXBIDS_NAME
	STORAGE_MAXBIDS_STREAMARN
	STORAGE_SERVERCONNECTIONS_ARN
	STORAGE_SERVERCONNECTIONS_NAME
	STORAGE_SERVERCONNECTIONS_STREAMARN
Amplify Params - DO NOT EDIT */
var AWS = require("aws-sdk");
AWS.config.update({ region: "eu-central-1" });
var express = require("express");
var bodyParser = require("body-parser");
var awsServerlessExpressMiddleware = require("aws-serverless-express/middleware");
var documentClient = new AWS.DynamoDB.DocumentClient();
var lambda = new AWS.Lambda();
var moment = require("moment-timezone");

const AUCTIONS_TABLE = process.env.STORAGE_DYNAMO0110890FSTAGING_NAME;
const BIDS_HISTORY_TABLE = "bidshistory-staging";
const MAXBID_TABLE = "maxbids-staging";
const apiGw = new AWS.ApiGatewayManagementApi({
  endpoint: "i3xsiueai3.execute-api.eu-central-1.amazonaws.com/production",
});
// declare a new express app
var app = express();
app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());

// Enable CORS for all methods
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

/**********************
 * Example get method *
 **********************/

app.get("/bids/:auctionid", async function (req, res) {
  // Add your code here
  let auction_id = req.params.auctionid;
  let bidhistory = await documentClient
    .query({
      TableName: BIDS_HISTORY_TABLE,
      KeyConditionExpression: "auction_id = :aid",
      ExpressionAttributeValues: {
        ":aid": auction_id,
      },
    })
    .promise()
    .catch((err) => {
      res.json({ success: "false", url: req.url, body: err });
      return;
    });
  res.json({ success: "true", url: req.url, body: bidhistory.Items });
});

/****************************
 * Example post method *
 ****************************/

app.post("/bids", async function (req, res) {
  // Add your code here
  let username =
    req.apiGateway.event.requestContext.authorizer.claims["cognito:username"];
  let auction_id = req.body.auction_id;
  let bid_amount = req.body.bid_amount;
  let bid_type = req.body.bid_type;
  let h_bid = null; //BID OBJECT

  //check if there is still time to bid
  let auction_object = await documentClient
    .get({
      TableName: AUCTIONS_TABLE,
      Key: { id: auction_id },
    })
    .promise();

  if (auction_object.Item === null || auction_object.Item === undefined) {
    res.json({ success: "false", url: req.url, body: "auction_not_found" });
    return;
  }

  console.log(auction_object);
  let cpEndtime = moment.tz(auction_object.Item.end, "Europe/Berlin");
  let cpCurtime = moment().tz("Europe/Berlin");

  console.log("CUR " + cpCurtime.format());
  console.log("END " + cpEndtime.format());

  if (cpEndtime.diff(cpCurtime) <= 0) {
    res.json({ success: "false", url: req.url, body: "auction_over" });
    return;
  }

  //get current bids
  console.log("getting all bids");
  let query_result = await documentClient
    .query({
      TableName: BIDS_HISTORY_TABLE,
      KeyConditionExpression: "auction_id = :aid",
      ExpressionAttributeValues: {
        ":aid": auction_id,
      },
    })
    .promise();
  let bids = query_result.Items;

  //check if new bid is high enough
  console.log(bids);
  var staffelung = 100;
  if (bids.length === 0) {
    console.log("no bids");
    if (bid_amount < 100) {
      res.json({ success: "false", url: req.url, body: "bid_too_low" });
      return;
    }
  } else {
    console.log("found bids");

    //Es gibt Gebote
    h_bid = bids[bids.length - 1];
    console.log("Highest bid:");
    console.log(h_bid);
    if (h_bid.bid_amount >= 20000) {
      staffelung = 500;
    } else if (h_bid.bid_amount >= 5000) {
      staffelung = 250;
    }
    if (parseFloat(bid_amount) < parseFloat(h_bid.bid_amount) + staffelung) {
      console.log("failed");
      res.json({ success: "false", url: req.url, body: "bid_too_low" });
      return;
    }
  }

  //bid is high enough
  //continuing...
  //enter bid

  let promises = [];

  if (bids.length === 0) {
    h_bid = {
      auction_id: auction_id,
      bid_amount: 0,
      username: "system",
    };
  }

  let entry_amount = bid_amount;
  //if bid is max bid, lower the amount
  if (bid_type === "max") {
    entry_amount = parseFloat(h_bid.bid_amount) + staffelung;
    promises.push(
      new Promise((resolve, reject) => {
        documentClient
          .put({
            TableName: MAXBID_TABLE,
            Item: {
              auction_id: auction_id,
              username: username,
              bid_amount: bid_amount,
            },
          })
          .promise()
          .then(() => {
            resolve();
          })
          .catch((err) => {
            if (err.code === "ConditionalCheckFailedException") {
              documentClient
                .update({
                  TableName: MAXBID_TABLE,
                  Key: { auction_id: auction_id, username: username },
                  UpdateExpression: "set bid_amount = :ba",
                  ExpressionAttributeValues: {
                    ":ba": bid_amount,
                  },
                })
                .promise()
                .then(() => {
                  resolve();
                });
            }
          });
      })
    );
  }

  let ts = moment().tz("Europe/Berlin").format("YYYY-MM-DDTHH:mm:ss.SSSZ");

  promises.push(
    documentClient
      .put({
        TableName: BIDS_HISTORY_TABLE,
        Item: {
          auction_id: auction_id,
          timestamp: ts,
          bid_type: bid_type,
          bid_amount: entry_amount,
          username: username,
        },
      })
      .promise()
  );

  promises.push(
    lambda
      .invoke({
        FunctionName: "LApriceupdate-staging",
        InvocationType: "Event",
        Payload: JSON.stringify({
          item: {
            price: entry_amount,
            action: "price_update",
            username: username,
            total_bid_amounts: bids.length + 1,
          },
          auction_id: auction_id,
        }),
      })
      .promise()
  );

  promises.push(
    lambda
      .invoke({
        FunctionName: "LAnewbid-staging",
        InvocationType: "Event",
        Payload: JSON.stringify({
          auction_id: auction_id,
          item: {
            action: "new_bid",
            username: username,
            timestamp: ts,
            bid_amount: entry_amount,
          },
        }),
      })
      .promise()
  );

  promises.push(
    lambda
      .invoke({
        FunctionName: "LAbidupdate-staging",
        InvocationType: "Event",
        Payload: JSON.stringify({
          auction_id: auction_id,
          total_bid_amounts: bids.length + 1,
          maxBid: entry_amount,
          username: username,
        }),
      })
      .promise()
  );

  await Promise.all(promises);
  promises = [];

  promises.push(
    lambda
      .invoke({
        FunctionName: "LAmaxbidupdate-staging",
        InvocationType: "Event",
        Payload: JSON.stringify({
          auction_id: auction_id,
          username: username,
          amount: entry_amount,
          total_bid_amounts: bids.length + 1,
        }),
      })
      .promise()
  );

  promises.push(
    lambda
      .invoke({
        FunctionName: "LAtimerreset-staging",
        InvocationType: "Event",
        Payload: JSON.stringify({
          auction_id: auction_id,
        }),
      })
      .promise()
  );

  await Promise.all(promises);
  console.log("sending json");
  res.json({ success: "true", url: req.url, body: "bid-placed" });
});
/****************************
 * Example put method *
 ****************************/

app.put("/bids", function (req, res) {
  // Add your code here
  res.json({ success: "put call succeed!", url: req.url, body: req.body });
});

app.put("/bids/*", function (req, res) {
  // Add your code here
  res.json({ success: "put call succeed!", url: req.url, body: req.body });
});

/****************************
 * Example delete method *
 ****************************/

app.delete("/bids", function (req, res) {
  // Add your code here
  res.json({ success: "delete call succeed!", url: req.url });
});

app.delete("/bids/*", function (req, res) {
  // Add your code here
  res.json({ success: "delete call succeed!", url: req.url });
});

app.listen(3000, function () {
  console.log("App started");
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app;
