/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

/* Amplify Params - DO NOT EDIT
	ENV
	REGION
	STORAGE_DYNAMO0110890FSTAGING_ARN
	STORAGE_DYNAMO0110890FSTAGING_NAME
	STORAGE_DYNAMO73822F32STAGING_ARN
	STORAGE_DYNAMO73822F32STAGING_NAME
Amplify Params - DO NOT EDIT */

var express = require("express");
var bodyParser = require("body-parser");
var awsServerlessExpressMiddleware = require("aws-serverless-express/middleware");
var AWS = require("aws-sdk");
AWS.config.update({ region: "eu-central-1" });
// declare a new express app
var app = express();
app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());
var documentClient = new AWS.DynamoDB.DocumentClient();

// Enable CORS for all methods
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

/**********************
 * Example get method *
 **********************/

app.get("/auctions", async function (req, res) {
  // Add your code here
  var tableName = "dynamo0110890f-staging";
  var params = {
    TableName: tableName,
    FilterExpression: "#status = :status",
    ExpressionAttributeNames: {
      "#status": "status",
    },
    ExpressionAttributeValues: {
      ":status": "active",
    },
  };
  await documentClient
    .scan(params)
    .promise()
    .then((response) => {
      res.json({
        success: "true",
        url: req.url,
        body: response,
      });
    })
    .catch((err) => {
      res.json({
        success: "false",
        url: req.url,
        body: err,
      });
    });
});

app.get("/auctions/:auctionid", async function (req, res) {
  return new Promise(async (resolve, reject) => {
    var tableName = "dynamo0110890f-staging";
    var params = {
      TableName: tableName,
      Key: {
        id: req.params.auctionid,
      },
    };
    await documentClient
      .get(params)
      .promise()
      .then(async (response) => {
        console.log("got response");
        if (response.Item !== null && response.Item !== undefined) {
          console.log("answering");
          res.json({ success: "true", url: req.url, body: response });
          console.log("answered");
          console.log("updating...");
          await documentClient
            .update({
              TableName: tableName,
              Key: { id: response.Item.id },
              UpdateExpression: "set #views = :views",
              ExpressionAttributeNames: {
                "#views": "views",
              },
              ExpressionAttributeValues: {
                ":views": response.Item.views + 1,
              },
            })
            .promise()
            .then(() => {
              resolve(true);
            });
        } else {
          res.json({
            success: "false",
            url: req.url,
            body: "Auction not found",
          });
        }
      })
      .catch((err) => {
        res.json({ success: "false", url: req.url, body: err });
      });
  });
});

/****************************
 * Example post method *
 ****************************/

app.post("/auctions", function (req, res) {
  // Add your code here
  res.json({ success: "post call succeed!", url: req.url, body: req.body });
});

app.post("/auctions/*", function (req, res) {
  // Add your code here
  res.json({ success: "post call succeed!", url: req.url, body: req.body });
});

/****************************
 * Example put method *
 ****************************/

app.put("/auctions", function (req, res) {
  // Add your code here
  res.json({ success: "put call succeed!", url: req.url, body: req.body });
});

app.put("/auctions/*", function (req, res) {
  // Add your code here
  res.json({ success: "put call succeed!", url: req.url, body: req.body });
});

/****************************
 * Example delete method *
 ****************************/

app.delete("/auctions", function (req, res) {
  // Add your code here
  res.json({ success: "delete call succeed!", url: req.url });
});

app.delete("/auctions/*", function (req, res) {
  // Add your code here
  res.json({ success: "delete call succeed!", url: req.url });
});

app.listen(3000, function () {
  console.log("App started");
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app;
