/* Amplify Params - DO NOT EDIT
	ENV
	REGION
	STORAGE_BIDSHISTORY_ARN
	STORAGE_BIDSHISTORY_NAME
	STORAGE_BIDSHISTORY_STREAMARN
	STORAGE_BIDS_ARN
	STORAGE_BIDS_NAME
	STORAGE_BIDS_STREAMARN
	STORAGE_SERVERCONNECTIONS_ARN
	STORAGE_SERVERCONNECTIONS_NAME
	STORAGE_SERVERCONNECTIONS_STREAMARN
Amplify Params - DO NOT EDIT */
const AWS = require("aws-sdk");
const ddb = new AWS.DynamoDB.DocumentClient({
  region: "eu-central-1",
});
const CON_TABLE = "serverconnections-staging";
const BID_TABLE = "bids-staging";
const BIDS_HISTORY_TABLE = "bidshistory-staging";

exports.handler = async (event) => {
  const apiGw = new AWS.ApiGatewayManagementApi({
    endpoint:
      event.requestContext.domainName + "/" + event.requestContext.stage,
  });
  return new Promise(async (resolve, reject) => {
    var data = JSON.parse(event.body);
    ddb.put(
      {
        TableName: CON_TABLE,
        Item: {
          auction_id: data.auction_id,
          connectionId: event.requestContext.connectionId,
        },
        ConditionExpression: "attribute_not_exists(connectionId)",
      },
      function (err, data) {
        if (err) {
          console.log(err);
        }
      }
    );
    let bids = await ddb
      .query({
        TableName: BIDS_HISTORY_TABLE,
        KeyConditionExpression: "auction_id = :aid",
        ExpressionAttributeValues: {
          ":aid": data.auction_id,
        },
      })
      .promise()
      .catch((err) => {
        console.log(err);
        return reject({
          statusCode: 500,
          body: err,
        });
      });

    let maxBid = 0;
    let maxUsername = "";
    let total_bid_amounts = 0;

    if (bids.Items.length > 0) {
      maxBid = bids.Items[bids.Items.length - 1].bid_amount;
      maxUsername = bids.Items[bids.Items.length - 1].username;
      total_bid_amounts = bids.Items.length;
    }
    await apiGw
      .postToConnection({
        ConnectionId: event.requestContext.connectionId,
        Data: JSON.stringify({
          action: "price_update",
          price: maxBid,
          username: maxUsername,
          total_bid_amounts: total_bid_amounts,
        }),
      })
      .promise()
      .catch((err) => {
        console.log(err);
        return reject({
          statusCode: 500,
          body: err,
        });
      });
    resolve({
      statusCode: 200,
      body: "Subscribed",
    });
  });
};
