import ApiHelper from "../helper/api";

export default class AuctionService {
  apiHelper;

  constructor() {
    this.apiHelper = new ApiHelper();
  }

  getAuction = async (auction_id) => {
    return this.apiHelper.getAuction(auction_id);
  };
}
