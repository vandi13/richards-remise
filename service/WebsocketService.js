export default class WebsocketService {
  store;
  constructor(store) {
    this.store = store;
  }
  handleMessage = (data) => {
    if (data.action === "price_update") {
      this.store.setPrice(parseFloat(data.price).toFixed(2));
      this.store.setHighestBidder(data.username);
      this.store.setBidAmount(data.total_bid_amounts);
    }
    if (data.action === "new_bid") {
      this.store.addToBidHistory(data);
    }
    if (data.action === "time_update") {
      this.store.updateEndTime(data.time);
    }
    if (data.action === "auction_ended") {
      this.store.setEnded(true);
    }
    if (data.action === "new_comment") {
      this.store.addComment({
        time: data.time,
        Username: data.username,
        Comment: data.comment,
      });
    }
  };
}
