import { API, Auth } from "aws-amplify";

export default class ApiHelper {
  getAllAuctions() {
    return API.get(process.env.NEXT_PUBLIC_API_NAME, "/auctions");
  }
  getAuction(auction_id) {
    return API.get(process.env.NEXT_PUBLIC_API_NAME, "/auctions/" + auction_id);
  }
  unfollowAuction(username, auction_id) {
    return API.post(process.env.NEXT_PUBLIC_API_NAME, "/follows", {
      body: {
        username: username,
        auction_id: auction_id,
        action: "unfollow",
      },
    });
  }
  followAuction(username, auction_id) {
    return API.post(process.env.NEXT_PUBLIC_API_NAME, "/follows", {
      body: {
        username: username,
        auction_id: auction_id,
        action: "follow",
      },
    });
  }
  getFollows(username) {
    return API.get(process.env.NEXT_PUBLIC_API_NAME, "/follows/" + username);
  }
  async getBidHistory(auction_id) {
    return API.get(process.env.NEXT_PUBLIC_API_NAME, "/bids/" + auction_id, {
      headers: {
        Authorization: `Bearer ${(await Auth.currentSession())
          .getIdToken()
          .getJwtToken()}`,
      },
    });
  }
  async sendBid(auction_id, bidAmount, bidMode) {
    return API.post(process.env.NEXT_PUBLIC_API_NAME, "/bids", {
      body: {
        auction_id: auction_id,
        bid_amount: bidAmount,
        bid_type: bidMode,
      },
      headers: {
        Authorization: `Bearer ${(await Auth.currentSession())
          .getIdToken()
          .getJwtToken()}`,
      },
    });
  }
  async verifyCreditCard() {
    return API.post(process.env.NEXT_PUBLIC_API_NAME, "/mollie", {
      body: {
        action: "firstpayment",
      },
      headers: {
        Authorization: `Bearer ${(await Auth.currentSession())
          .getIdToken()
          .getJwtToken()}`,
      },
    });
  }
  async createComment(auction_id, pComment) {
    return API.post(process.env.NEXT_PUBLIC_API_NAME, "/comments", {
      headers: {
        Authorization: `Bearer ${(await Auth.currentSession())
          .getIdToken()
          .getJwtToken()}`,
      },
      body: {
        auction_id: auction_id,
        comment: pComment,
      },
    });
  }
  async createMollieAccount() {
    return API.get(process.env.NEXT_PUBLIC_API_NAME, "/mollie", {
      headers: {
        Authorization: `Bearer ${(await Auth.currentSession())
          .getIdToken()
          .getJwtToken()}`,
      },
    });
  }
}
