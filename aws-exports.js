/* eslint-disable */
// WARNING: DO NOT EDIT. This file is automatically generated by AWS Amplify. It will be overwritten.

const awsmobile = {
    "aws_project_region": "eu-central-1",
    "aws_cognito_identity_pool_id": "eu-central-1:cff5d295-0324-4c35-be89-2dba81cf2d01",
    "aws_cognito_region": "eu-central-1",
    "aws_user_pools_id": "eu-central-1_hMLMEUgFD",
    "aws_user_pools_web_client_id": "3idb1tlr5b4hsfbg2n2oc7s3uh",
    "oauth": {},
    "aws_cognito_login_mechanisms": [
        "PREFERRED_USERNAME"
    ],
    "aws_cognito_signup_attributes": [
        "EMAIL"
    ],
    "aws_cognito_mfa_configuration": "OFF",
    "aws_cognito_mfa_types": [
        "SMS"
    ],
    "aws_cognito_password_protection_settings": {
        "passwordPolicyMinLength": 8,
        "passwordPolicyCharacters": []
    },
    "aws_cognito_verification_mechanisms": [
        "EMAIL"
    ],
    "aws_user_files_s3_bucket": "remisestorage01812-staging",
    "aws_user_files_s3_bucket_region": "eu-central-1",
    "aws_dynamodb_all_tables_region": "eu-central-1",
    "aws_dynamodb_table_schemas": [
        {
            "tableName": "dynamo0110890f-staging",
            "region": "eu-central-1"
        },
        {
            "tableName": "dynamo73822f32-staging",
            "region": "eu-central-1"
        },
        {
            "tableName": "dynamo29f3aeb7-staging",
            "region": "eu-central-1"
        },
        {
            "tableName": "serverconnections-staging",
            "region": "eu-central-1"
        },
        {
            "tableName": "bids-staging",
            "region": "eu-central-1"
        },
        {
            "tableName": "bidshistory-staging",
            "region": "eu-central-1"
        },
        {
            "tableName": "maxbids-staging",
            "region": "eu-central-1"
        }
    ],
    "aws_cloud_logic_custom": [
        {
            "name": "api4805bf44",
            "endpoint": "https://f25dt7lbu1.execute-api.eu-central-1.amazonaws.com/staging",
            "region": "eu-central-1"
        }
    ]
};


export default awsmobile;
