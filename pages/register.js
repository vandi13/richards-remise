import Auth from "@aws-amplify/auth";
import { observer } from "mobx-react-lite";
import React, { useState } from "react";
import {
  Card,
  Col,
  Container,
  Form,
  Row,
  Button,
  Spinner,
} from "react-bootstrap";
import SiteWrapper from "../components/siteWrapper";
import { useAlert } from "react-alert";
import Link from "next/link";
import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
const data = new Object();

const RegisterPage = observer(() => {
  const alert = useAlert();
  const router = useRouter();

  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);

  const updateProperty = (key, value) => {
    data[key] = value;
  };
  const onChange = (event) => {
    updateProperty(event.target.name, event.target.value);
  };
  const handleRegister = async () => {
    if (
      data.username === undefined ||
      data.password === undefined ||
      data.email === undefined ||
      data.firstname === undefined ||
      data.lastname === undefined
    ) {
      alert.show("Bitte Daten eingeben", { type: "error" });
      return;
    }
    setLoading(true);
    try {
      let username = data.username;
      let password = data.password;
      let email = data.email;
      let firstname = data.firstname;
      let lastname = data.lastname;
      const { user } = await Auth.signUp({
        username,
        password,
        attributes: {
          email,
          given_name: firstname,
          family_name: lastname,
        },
      });
      alert.show("Erfolg", { type: "success" });
      setLoading(false);
      setSuccess(true);
    } catch (error) {
      alert.show(error.message, { type: "error" });
      setLoading(false);
    }
  };

  return (
    <SiteWrapper>
      <Container size="xxl">
        <Row className="mb-3 mt-5">
          <Col className="text-center">
            Bereits registriert?{" "}
            <Link href="/login">
              <a>
                <span style={{ color: "darkgreen" }}>Hier einloggen</span>
              </a>
            </Link>
          </Col>
        </Row>
        <Row className="mb-3">
          <Col>
            <Card style={{ zIndex: "0" }}>
              <Card.Body className="shadow-sm">
                <h2>Account erstellen</h2>
                <Form.Group className="mb-3" controlId="fbBn">
                  <Form.Label>Benutzername</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Username"
                    onChange={onChange}
                    name="username"
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="fbPw">
                  <Form.Label>Passwort</Form.Label>
                  <Form.Control
                    name="password"
                    onChange={onChange}
                    type="password"
                    placeholder="Passwort"
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="fbEm">
                  <Form.Label>E-Mail</Form.Label>
                  <Form.Control
                    name="email"
                    onChange={onChange}
                    type="email"
                    placeholder="E-Mail"
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="fbEm1">
                  <Form.Label>Vorname</Form.Label>
                  <Form.Control
                    name="firstname"
                    onChange={onChange}
                    type="text"
                    placeholder="Vorname"
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="fbEm2">
                  <Form.Label>Nachname</Form.Label>
                  <Form.Control
                    name="lastname"
                    onChange={onChange}
                    type="text"
                    placeholder="Nachname"
                  />
                </Form.Group>
                <Button
                  variant="primary"
                  onClick={handleRegister}
                  disabled={loading || success}
                >
                  {loading ? (
                    <Spinner animation="border" as="span" size="sm" />
                  ) : (
                    <>
                      {success ? (
                        <>
                          <FontAwesomeIcon icon={faCheck} /> Erfolg
                        </>
                      ) : (
                        <>Absenden</>
                      )}
                    </>
                  )}
                </Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </SiteWrapper>
  );
});
export default RegisterPage;
