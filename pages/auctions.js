import React from "react";
import SiteWrapper from "../components/siteWrapper";
import AuctionList from "../components/auctionList";

export default function Auctions() {
  return (
    <SiteWrapper>
      <AuctionList title={true} />
    </SiteWrapper>
  );
}
