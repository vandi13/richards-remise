import "../styles/globals.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { transitions, positions, Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import React from "react";
import awsExports from "../aws-exports";
import Amplify, { Auth } from "aws-amplify";
import Head from "next/head";
import { MediaContextProvider } from "../helper/Media";
import { SSRProvider } from "react-bootstrap";

Amplify.configure({ ...awsExports });

const options = {
  // you can also just use 'bottom center'
  position: positions.BOTTOM_CENTER,
  timeout: 5000,
  offset: "30px",
  // you can also just use 'scale'
  transition: transitions.SCALE,
};

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width,initial-scale=1" />
      </Head>
      <SSRProvider>
        <AlertProvider template={AlertTemplate} {...options}>
          <MediaContextProvider>
            <Component {...pageProps} />
          </MediaContextProvider>
        </AlertProvider>
      </SSRProvider>
    </>
  );
}

export default MyApp;
