import { observer } from "mobx-react";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { Col, Container, Row, Spinner } from "react-bootstrap";
import SiteWrapper from "../components/siteWrapper";
import ProfileView from "../components/my/profileView";
import UserStore from "../store/UserStore";
const userStore = new UserStore();

const MeineRemise = observer(({}) => {
  let router = useRouter();

  console.log(typeof window);
  useEffect(() => {
    if (userStore.ready && !userStore.loggedIn) {
      router.push("/login");
    }
  }, [userStore.loggedIn, userStore.ready]);

  useEffect(() => {
    userStore.init();
  }, []);

  if (!userStore.loggedIn) {
    return (
      <SiteWrapper>
        <Spinner
          animation="border"
          style={{
            position: "absolute",
            top: "calc( ( 100vh - 200px ) / 2 - 15px)",
            left: "calc(100vw / 2 - 15px )",
          }}
        />
      </SiteWrapper>
    );
  }
  return (
    <SiteWrapper>
      <Container size="xxl">
        <Row>
          <Col>
            <ProfileView userStore={userStore} />
          </Col>
        </Row>
      </Container>
    </SiteWrapper>
  );
});

export default MeineRemise;
