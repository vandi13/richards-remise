import Auth from "@aws-amplify/auth";
import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import {
  Card,
  Col,
  Container,
  Form,
  Row,
  Button,
  Spinner,
} from "react-bootstrap";
import SiteWrapper from "../components/siteWrapper";
import { useAlert } from "react-alert";
import Link from "next/link";
import { useRouter } from "next/router";
const data = new Object();

const LoginPage = observer(() => {
  const alert = useAlert();
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    Auth.currentAuthenticatedUser()
      .then((user) => router.push("/"))
      .catch((err) => {});
  }, [Auth]);

  const updateProperty = (key, value) => {
    data[key] = value;
  };
  const onChange = (event) => {
    updateProperty(event.target.name, event.target.value);
  };
  const handleLogin = async () => {
    if (data.username === undefined || data.password === undefined) {
      alert.show("Bitte Daten eingeben", { type: "error" });
      return;
    }
    setLoading(true);
    try {
      const user = await Auth.signIn(data.username, data.password);
      setLoading(false);
      router.push("/");
    } catch (error) {
      console.log(error);
      alert.show("Login fehlgeschlagen", { type: "error" });
      setLoading(false);
    }
  };

  return (
    <SiteWrapper>
      <Container size="xxl">
        <Row className="mb-3 mt-5">
          <Col className="text-center">
            Noch nicht registriert?{" "}
            <Link href="/register">
              <a>
                <span style={{ color: "darkgreen" }}>Jetzt registrieren</span>
              </a>
            </Link>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card style={{ zIndex: "0" }}>
              <Card.Body className="shadow-sm">
                <h2>Einloggen</h2>
                <Form>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Benutzername</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter Username"
                      onChange={onChange}
                      name="username"
                    />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Passwort</Form.Label>
                    <Form.Control
                      name="password"
                      onChange={onChange}
                      type="password"
                      placeholder="Passwort"
                    />
                  </Form.Group>
                  <Button
                    variant="primary"
                    onClick={handleLogin}
                    disabled={loading}
                  >
                    {!loading ? (
                      <>Absenden</>
                    ) : (
                      <>
                        <Spinner animation="border" size="sm" as="span" />
                      </>
                    )}
                  </Button>
                </Form>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </SiteWrapper>
  );
});
export default LoginPage;
