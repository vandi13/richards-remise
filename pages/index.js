import React from "react";
import SiteWrapper from "../components/siteWrapper";
import AuctionList from "../components/auctionList";
import { Col, Container, Image, Row } from "react-bootstrap";
import { Media } from "../helper/Media";
import HomeMobile from "../components/home/homeMobile";

export default function Home() {
  return (
    <SiteWrapper>
      <Media at="xs">
        <HomeMobile />
      </Media>
      <Media greaterThan="xs">
        <div
          className="w-100 d-flex justify-content-center align-items-center flex-column"
          style={{
            height: "750px",
            backgroundImage: "url('/bigbg.webp')",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}
        >
          <Image
            src="/logobig.webp"
            style={{ width: "600px", height: "424px" }}
          />
          <span style={{ fontSize: "21px", color: "#3D5D54" }}>
            Einfach Ihren Klassiker verkaufen lassen
          </span>
          <div className="mt-4">
            <Image src="/partner1.webp" className="m-3" />
            <Image src="/partner2.webp" className="m-3" />
          </div>
        </div>
        <Container className="mt-5 mb-5">
          <Row className="text-center">
            <Col>
              <span style={{ fontSize: "28px" }}>UNSERE KOLLEKTION</span>
              <br />
              <br />
              <div style={{ width: "644px", margin: "0 auto" }}>
                Schauen Sie sich gerne um, inspizieren Sie unsere Exponate. In
                Richards Remise bieten wir Ihnen einen Feinkostladen für
                Automobilisten, eine Boutique für jeden Benzin-Liebhaber. Von
                Fotos, über Videos bis hin zu 3D Ausstellung des Innenraums ist
                alles dabei.
              </div>
            </Col>
          </Row>
        </Container>
        <AuctionList title={false} />
      </Media>
    </SiteWrapper>
  );
}
