import { observer } from "mobx-react-lite";
import { useRouter } from "next/dist/client/router";
import React, { useEffect, useState } from "react";
import { Container, Image, Spinner } from "react-bootstrap";
import AuctionBidInfo from "../../components/auctionBidInfo";
import BidContainer from "../../components/bidContainer";
import CarInfo from "../../components/carInfo";
import CommentSection from "../../components/commentSection";
import SiteWrapper from "../../components/siteWrapper";
import SingleAuctionStore from "../../store/SingleAuctionStore";
import UserStore from "../../store/UserStore";
import useWebSocket from "react-use-websocket";
import WebsocketService from "../../service/WebsocketService";
import { Media } from "../../helper/Media";
const socketUrl =
  "wss://i3xsiueai3.execute-api.eu-central-1.amazonaws.com/production";

const store = new SingleAuctionStore();
const userStore = new UserStore();

const AuctionPage = observer(() => {
  const router = useRouter();
  const { auctionid } = router.query;
  const [showBidContainer, setShowBidContainer] = useState(false);
  const websocketService = new WebsocketService(store);

  const {
    sendMessage,
    sendJsonMessage,
    lastMessage,
    lastJsonMessage,
    readyState,
    getWebSocket,
  } = useWebSocket(socketUrl, {
    onOpen: () => {
      console.log("connected");
    },
    onMessage: (msg) => {
      let data = JSON.parse(msg.data);
      websocketService.handleMessage(data);
    },
    onClose: () => {
      console.log("disconnected");
    },
    //Will attempt to reconnect on all close events, such as server shutting down
    shouldReconnect: (closeEvent) => true,
  });

  useEffect(() => {
    userStore.init();
  }, []);
  useEffect(() => {
    if (auctionid === undefined) return;
    store.intiializeAuction(auctionid).then(() => {
      sendMessage(
        JSON.stringify({
          action: "subscribe",
          auction_id: auctionid,
        })
      );
    });
  }, [auctionid]);

  if (!store.ready || !userStore.ready) {
    return (
      <SiteWrapper>
        <Spinner
          animation="border"
          style={{
            position: "absolute",
            top: "calc( ( 100vh - 200px ) / 2 - 15px)",
            left: "calc(100vw / 2 - 15px )",
          }}
        />
      </SiteWrapper>
    );
  }

  return (
    <SiteWrapper>
      <Media at="xs">
        <div
          style={{
            height: "3px",
            border: "none",
            backgroundColor: "rgb(61, 93, 84)",
            zIndex: "2",
          }}
        ></div>
        <Image fluid src={store.thumbnail} />
      </Media>

      <Container size="xxl">
        <AuctionBidInfo
          store={store}
          userStore={userStore}
          setShowBidContainer={setShowBidContainer}
        />
      </Container>

      <div
        style={{
          height: "2px",
          border: "none",
          backgroundColor: "#f5f5f5",
        }}
        className="w-100 mt-4 mb-4"
      />

      <Container size="xxl">
        <CarInfo store={store} userStore={userStore} />
      </Container>

      <Container size="xxl">
        <CommentSection store={store} userStore={userStore} />
      </Container>

      {showBidContainer && (
        <BidContainer store={store} setShowBidContainer={setShowBidContainer} />
      )}
    </SiteWrapper>
  );
});
export default AuctionPage;
