import Storage from "@aws-amplify/storage";
import {
  action,
  flow,
  flowResult,
  makeObservable,
  observable,
  runInAction,
} from "mobx";

export default class AuctionStore {
  auction;
  thumbnail;
  ready;

  constructor(pAuction) {
    makeObservable(this, {
      auction: observable,
      thumbnail: observable,
      ready: observable,
    });
    this.auction = pAuction;
    this.ready = false;

    if (this.auction.thumbnail === "none") {
      this.thumbnail = "/auto.jpg";
      this.ready = true;
    } else {
      Storage.get(this.auction.thumbnail).then((url) => {
        runInAction(() => {
          this.thumbnail = url;
          this.ready = true;
        });
      });
    }
  }
}
