import Auth from "@aws-amplify/auth";
import {
  action,
  computed,
  makeObservable,
  observable,
  runInAction,
} from "mobx";
import ApiHelper from "../helper/api";

const apiHelper = new ApiHelper();

export default class UserStore {
  user;
  follows = [];
  ready = false;
  followLoading = false;
  loggedIn = false;
  constructor() {
    makeObservable(this, {
      user: observable,
      ready: observable,
      loggedIn: observable,
      follows: observable,
      followAuction: action,
      followLoading: observable,
      unfollowAuction: action,
    });
  }

  init = () => {
    Auth.currentAuthenticatedUser()
      .then((user) => {
        console.log("is");
        runInAction(() => {
          console.log(user);
          this.user = user;
          this.loggedIn = true;
          this.ready = true;
        });

        apiHelper.getFollows(this.user.username).then((res) => {
          if (res.success === "false") return;
          if (res.body.Item === undefined) return;
          if (res.body.Item.followed_auctions.length > 0) {
            runInAction(() => {
              this.follows = res.body.Item.followed_auctions;
              console.log("follows loaded");
            });
          }
        });
      })
      .catch(() => {
        console.log("err");
        runInAction(() => {
          this.user = null;
          this.ready = true;
          this.loggedIn = false;
          console.log("ready");
        });
      });
  };

  unfollowAuction = (auction_id) => {
    runInAction(() => {
      this.followLoading = true;
    });
    apiHelper.unfollowAuction(this.user.username, auction_id).then((res) => {
      runInAction(() => {
        this.follows = this.follows.filter(
          (follow) => follow.id !== auction_id
        );
        this.followLoading = false;
      });
    });
  };

  followAuction = (auction_id) => {
    runInAction(() => {
      this.followLoading = true;
    });
    apiHelper.followAuction(this.user.username, auction_id).then((res) => {
      runInAction(() => {
        this.followLoading = false;
        if (res.success === "true") {
          this.follows = [auction_id, ...this.follows];
        }
      });
    });
  };
}
