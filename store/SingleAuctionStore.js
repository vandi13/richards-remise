import Storage from "@aws-amplify/storage";
import { action, makeObservable, observable, runInAction } from "mobx";
import ApiHelper from "../helper/api";
import AuctionService from "../service/AuctionService";

const apiHelper = new ApiHelper();

export default class SingleAuctionStore {
  auctionService;
  ready;
  auction;
  thumbnail;
  commentSending;
  bidHistory;
  bidHistoryReady;
  ended;
  constructor() {
    this.auctionService = new AuctionService();
    this.ready = false;
    this.commentSending = false;
    this.bidHistoryReady = false;
    this.ended = false;

    makeObservable(this, {
      ready: observable,
      auction: observable,
      thumbnail: observable,
      intiializeAuction: action,
      sendComment: action,
      commentSending: observable,
      bidHistory: observable,
      bidHistoryReady: observable,
      setPrice: action,
      setHighestBidder: action,
      setBidAmount: action,
      addToBidHistory: action,
      ended: observable,
      updateEndTime: action,
      setEnded: action,
      addComment: action,
    });
  }

  addComment = (comment) => {
    runInAction(() => {
      this.auction.comments = [...this.auction.comments, comment];
    });
  };

  setEnded = (ended) => {
    runInAction(() => {
      this.ended = ended;
    });
  };

  updateEndTime = (time) => {
    runInAction(() => {
      this.auction.end = time;
    });
  };

  addToBidHistory = (bid) => {
    runInAction(() => {
      this.bidHistory = [bid, ...this.bidHistory];
    });
  };

  setBidAmount = (amount) => {
    runInAction(() => {
      this.auction.bid_amount = amount;
    });
  };

  setHighestBidder = (bidder) => {
    runInAction(() => {
      this.auction.highest_bidder = bidder;
    });
  };

  setPrice = (price) => {
    runInAction(() => {
      this.auction.current_bid = price;
    });
  };

  sendComment = (comment) => {
    runInAction(() => {
      this.commentSending = true;
    });
    apiHelper.createComment(this.auction.id, comment).then(() => {
      runInAction(() => {
        this.commentSending = false;
      });
    });
  };

  intiializeAuction = async (auction_id) => {
    let result = await this.auctionService.getAuction(auction_id);
    let url = await Storage.get(result.body.Item.thumbnail);
    runInAction(() => {
      this.auction = result.body.Item;
      this.thumbnail = url;
      this.ready = true;
    });

    let res = await apiHelper.getBidHistory(auction_id);
    runInAction(() => {
      this.bidHistory = res.body.reverse();
      this.bidHistoryReady = true;
    });
  };
}
