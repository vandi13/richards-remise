import { action, makeObservable, observable, runInAction } from "mobx";
import ApiHelper from "../helper/api";

const apiHelper = new ApiHelper();

export default class BidStore {
  bidMode;
  bidAmount;
  inputVisible;
  bidSending;
  bidSentSuccessfully;
  constructor() {
    this.bidMode = "live";
    this.bidAmount = "0";
    this.inputVisible = false;
    this.bidSending = false;
    this.bidSentSuccessfully = false;
    makeObservable(this, {
      bidMode: observable,
      bidAmount: observable,
      setBidMode: action,
      setBidAmount: action,
      inputVisible: observable,
      setInputVisible: action,
      sendBid: action,
      bidSending: observable,
      bidSentSuccessfully: observable,
    });
  }

  sendBid = (auction_id) => {
    runInAction(() => {
      this.bidSending = true;
    });
    apiHelper
      .sendBid(auction_id, this.bidAmount, this.bidMode)
      .then((response) => {
        runInAction(() => {
          this.bidSending = false;
          if (response.success === "true") {
            this.bidSentSuccessfully = true;
            setTimeout(() => {
              runInAction(() => {
                this.bidSentSuccessfully = false;
              });
            }, 3000);
          }
        });
      });
  };

  setBidMode = (bid_mode) => {
    runInAction(() => {
      this.bidMode = bid_mode;
    });
  };

  setBidAmount = (bid_amount) => {
    runInAction(() => {
      this.bidAmount = bid_amount;
    });
  };

  setInputVisible = (p) => {
    runInAction(() => {
      this.inputVisible = p;
    });
  };
}
