import Storage from "@aws-amplify/storage";
import { action, makeObservable, observable, runInAction } from "mobx";
import ApiHelper from "../helper/api";

const apiHelper = new ApiHelper();

export default class FollowedAuctionsStore {
  followedAuctions = [];
  ready = false;
  constructor() {
    makeObservable(this, {
      ready: observable,
      followedAuctions: observable,
      init: action,
    });
  }
  init = (ids) => {
    runInAction(() => {
      this.ready = false;
      this.followedAuctions = [];
    });
    ids.forEach((id) => {
      apiHelper.getAuction(id.id).then((result) => {
        if (result.success !== "false") {
          if (result.body.Item.thumbnail !== "none") {
            Storage.get(result.body.Item.thumbnail).then((url) => {
              let item = result.body.Item;
              item.thumbnail = url;
              runInAction(() => {
                this.followedAuctions = [...this.followedAuctions, item];
                this.ready = true;
              });
            });
          } else {
            runInAction(() => {
              this.followedAuctions = [
                ...this.followedAuctions,
                result.body.Item,
              ];
              this.ready = true;
            });
          }
        }
      });
    });
  };
}
