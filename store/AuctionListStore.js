import React from "react";
import ReactDOM from "react-dom";
import {
  action,
  makeAutoObservable,
  makeObservable,
  observable,
  runInAction,
} from "mobx";
import { observer } from "mobx-react-lite";

export default class AuctionListStore {
  auctions;

  constructor() {
    makeObservable(this, {
      auctions: observable,
      addAuction: action,
      clearAuctions: action,
    });
    this.auctions = [];
  }

  clearAuctions() {
    runInAction(() => {
      this.auctions = [];
    });
  }

  addAuction(auction) {
    runInAction(() => {
      this.auctions.push(auction);
    });
  }
}
